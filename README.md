Installation:
ROS: http://wiki.ros.org/ROS/Installation

Packages needed:
  DWA Local Planner,
  Slam_gmapping,
  Turtlebot3,
  Turtlebot3_applications,
  Turtlebot3_applications_msgs

Launching:

Task 1 (Mapping):
roslaunch fira_maze start.launch
roslaunch fira_maze task1.launch

Task 2 (Navigation):
roslaunch fira_maze start.launch
roslaunch fira_maze task2.launch

Task 3 (Rescue Operation):
roslaunch fira_maze start3.launch
roslaunch fira_maze task3.launch

Task 4 (Restaurant simulation):
roslaunch fira_maze start4.launch
roslaunch fira_maze task4.launch

    
