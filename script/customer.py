#!/usr/bin/env python

import rospy, time
from sensor_msgs.msg import LaserScan
from geometry_msgs.msg import Twist
from std_msgs.msg import String
import numpy as np
from geometry_msgs.msg import PoseStamped, PoseWithCovarianceStamped
from move_base_msgs.msg import MoveBaseActionGoal
from actionlib_msgs.msg import GoalStatusArray, GoalStatusArray
from pyquaternion import Quaternion
import sys

class Customer:

    def comm_callback(self, msg):
        self.waiter = msg.data

    def __init__(self):
        rospy.loginfo('Customer node initialized')
        self.orders = 0
        self.stuff = "Nothing"
        self.waiter = "Empty"
        self.comm_sub = rospy.Subscriber('/comm', String, self.comm_callback)
        self.comm_pub = rospy.Publisher('/comm', String, queue_size=1)
        self.order()

    def order(self):
        time.sleep(2)
        while not rospy.is_shutdown():
            if self.waiter == "What can I get you?":
                if self.orders == 0:
                    self.stuff = 'Water'
                elif self.orders == 1:
                    self.stuff = 'Food'
                elif self.orders == 2:
                    self.stuff = 'Napkins'
                elif self.orders == 3:
                    self.stuff = 'Beer'
                else:
                    pass
                self.orders += 1
                self.comm_pub.publish(self.stuff)
                time.sleep(2)
            else:
                pass
        else: 
            print("Not entering customer loop!")

def main():

    rospy.init_node('customer', anonymous=True)

    try:
        customer = Customer()
    except rospy.ROSInterruptException:
        pass

if __name__ == '__main__':
    main()