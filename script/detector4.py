#!/usr/bin/env python
 
import rospy, time
from geometry_msgs.msg import Twist
from sensor_msgs.msg import LaserScan
from std_msgs.msg import String


def scan_callback(msg):
    global range_front
    global range_right
    global range_left
    global range_back
    global ranges
    global min_front,i_front, min_right,i_right, min_left ,i_left, min_back, i_back
    
    ranges = msg.ranges
    # get the range of a few points
    # in front of the robot (between 5 to -5 degrees)
    range_front[:15] = msg.ranges[15:0:-1]  
    range_front[15:] = msg.ranges[-1:-15:-1]
    # to the right (between 300 to 345 degrees)
    range_right = msg.ranges[300:345]
    # to the left (between 15 to 60 degrees)
    range_left = msg.ranges[60:15:-1]
    range_back = msg.ranges[165:195:1]
    # get the minimum values of each range 
    # minimum value means the shortest obstacle from the robot
    min_range,i_range = min( (ranges[i_range],i_range) for i_range in xrange(len(ranges)) )
    min_front,i_front = min( (range_front[i_front],i_front) for i_front in xrange(len(range_front)) )
    min_right,i_right = min( (range_right[i_right],i_right) for i_right in xrange(len(range_right)) )
    min_left ,i_left  = min( (range_left [i_left ],i_left ) for i_left  in xrange(len(range_left )) )
    min_back ,i_back  = min( (range_left [i_back ],i_back ) for i_back  in xrange(len(range_back )) )
    
# Initialize all variables
range_front = []
range_right = []
range_left  = []
range_back  = []
min_front = 0
i_front = 0
min_right = 0
i_right = 0
min_left = 0
i_left = 0
min_back = 0
i_back = 0

# Get name of the robot
# robot_name = rospy.get_param("/maze_follower/robot_name")
rospy.init_node('detector4', anonymous=True)
node_name = rospy.get_name()
if node_name == "/detector1":
    robot_name = "tb3_3"
elif node_name == "/detector2":
    robot_name = "tb3_4"
elif node_name == "/detector3":
    robot_name = "tb3_5"
elif node_name == "/detector4":
    robot_name = "tb3_6"


# Create the node
scan_sub = rospy.Subscriber(robot_name + '/scan', LaserScan, scan_callback)   # to read the laser scanner
comm_pub = rospy.Publisher('/comm', String, queue_size=10)



command = Twist()
command.linear.x = 0.0
command.angular.z = 0.0

msg = String()
        
rate = rospy.Rate(10)
time.sleep(2) # wait for node to initialize

t = 0.05
stuff = "Nothing"

# if robot_name == "tb3_3": stuff = "Water"
# elif robot_name == "tb3_4": stuff = "Food"
# elif robot_name == "tb3_5": stuff = "Napkins"
# elif robot_name == "tb3_6": stuff = "Beer"

while not rospy.is_shutdown():
    # update old values from scanner
    old_front = min_front
    old_right = min_right
    old_left = min_left
    old_back = min_back
    while not rospy.is_shutdown():
        if(abs(min_front-old_front)<t and abs(min_left-old_left)<t and abs(min_right-old_right)<t and abs(min_back-old_back)<t):
            if robot_name =="tb3_4":
                print(abs(min_front-old_front))
            pass
        else:
            comm_pub.publish("Here it is")
            
            time.sleep(30)
        time.sleep(2)

    rate.sleep()

