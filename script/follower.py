#!/usr/bin/env python
#################################################################################
# Copyright 2018 ROBOTIS CO., LTD.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#################################################################################

import rospy
import os
import pickle
from sensor_msgs.msg import LaserScan
from geometry_msgs.msg import Twist
from std_msgs.msg import String
import numpy as np

class follower:
    def __init__(self):
        rospy.loginfo('Follower node initialized')
        self.config_dir = os.path.join(os.path.dirname(__file__))
        self.pub = rospy.Publisher('/tb3_2/cmd_vel', Twist, queue_size = 1)
        self.clf = pickle.load(open('/home/ubuntu/catkin_ws/src/turtlebot3_applications/turtlebot3_follower/config/clf', "rb"))
        self.clf2 = pickle.load(open('/home/ubuntu/catkin_ws/src/turtlebot3_applications/turtlebot3_follower/config/clf2', "rb"))
        self.labels = {'30_0':0, '30_l':1, '30_r':2, '45_0':3, '45_l':4, '45_r':5,'15_0':6, 'empty':7}
        rospy.loginfo('Tree initialized')
        self.follow()

    def scan_callback(self, msg):
        
        self.ranges = msg.ranges
        # get the range of a few points
        # in front of the robot (between 5 to -5 degrees)
        self.range_front[:15] = msg.ranges[15:0:-1]  
        self.range_front[15:] = msg.ranges[-1:-15:-1]
        # to the right (between 300 to 345 degrees)
        self.range_right = msg.ranges[300:345]
        # to the left (between 15 to 60 degrees)
        self.range_left = msg.ranges[60:15:-1]
        self.range_back = msg.ranges[165:195:1]
        # get the minimum values of each range 
        # minimum value means the shortest obstacle from the robot
        self.min_range,i_range = min( (ranges[i_range],i_range) for i_range in xrange(len(ranges)) )
        self.min_front,i_front = min( (range_front[i_front],i_front) for i_front in xrange(len(range_front)) )
        self.min_right,i_right = min( (range_right[i_right],i_right) for i_right in xrange(len(range_right)) )
        self.min_left ,i_left  = min( (range_left [i_left ],i_left ) for i_left  in xrange(len(range_left )) )
        self.min_back ,i_back  = min( (range_left [i_back ],i_back ) for i_back  in xrange(len(range_back )) )

    def laser_scan(self):
        data_test=[]
        data_test_set=[]
        self.msg = rospy.wait_for_message("tb3_2/scan", LaserScan)

        for i in range(70,-2,-1) + range(359, 289,-1):

            if   np.nan_to_num( self.msg.ranges[i] ) != 0 :
                 data_test.append(np.nan_to_num(self.msg.ranges[i]))

            elif (i+1) in range(70,-2,-1) + range(359, 289,-1) and (i-1) in range(70,-2,-1) + range(359, 289,-1) and np.nan_to_num(self.msg.ranges[i]) == 0:
                 data_test.append((np.nan_to_num(self.msg.ranges[i+1])+np.nan_to_num(self.msg.ranges[i-1]))/2)

            else :
                 data_test.append(np.nan_to_num(self.msg.ranges[i]))

        data_test_set.append(data_test)

        return [x for (x , y) in self.labels.iteritems() if y == self.clf2.predict(data_test_set) ] 

    def follow(self):
        while not rospy.is_shutdown():

                x = self.laser_scan()
                twist = Twist()
                ## Do something according to each position##
                if  x == ['30_0']:
                    twist.linear.x  = 0.13
                    twist.angular.z = 0.0
                elif x== ['30_l']:
                    twist.linear.x  = 0.10
                    twist.angular.z = 0.4
                elif x== ['30_r']:
                    twist.linear.x  = 0.10
                    twist.angular.z = -0.4
                elif x== ['45_0']:
                    twist.linear.x  = 0.13
                    twist.angular.z = 0.0
                elif x== ['45_l']:
                    twist.linear.x  = 0.10
                    twist.angular.z = 0.3
                elif x== ['45_r']:
                    twist.linear.x  = 0.10
                    twist.angular.z = -0.3
                elif x== ['15_0']:
                    twist.linear.x  = 0.0
                    twist.angular.z = 0.0
                elif x== ['empty']:
                    twist.linear.x  = 0.0
                    twist.angular.z = 0.0
                else:
                    twist.linear.x  = 0.0
                    twist.angular.z = 0.0
                print(x)
                self.pub.publish(twist)

                # else:
                #     twist.linear.x  = 0.0; 		twist.angular.z = 0.4;


def main():

    rospy.init_node('follower', anonymous=True)

    try:
        follow = follower()
    except rospy.ROSInterruptException:
        pass    #print("Shutting down")

if __name__ == '__main__':
    main()