#!/usr/bin/env python
 
import rospy, time
from geometry_msgs.msg import Twist
from sensor_msgs.msg import LaserScan
from std_msgs.msg import String
import follower


def scan_callback(msg):
    global range_front
    global range_right
    global range_left
    global range_back
    global ranges
    global min_front,i_front, min_right,i_right, min_left ,i_left, min_back, i_back
    
    ranges = msg.ranges
    # get the range of a few points
    # in front of the robot (between 5 to -5 degrees)
    range_front[:15] = msg.ranges[15:0:-1]  
    range_front[15:] = msg.ranges[-1:-15:-1]
    # to the right (between 300 to 345 degrees)
    range_right = msg.ranges[300:345]
    # to the left (between 15 to 60 degrees)
    range_left = msg.ranges[60:15:-1]
    range_back = msg.ranges[165:195:1]
    # get the minimum values of each range 
    # minimum value means the shortest obstacle from the robot
    min_range,i_range = min( (ranges[i_range],i_range) for i_range in xrange(len(ranges)) )
    min_front,i_front = min( (range_front[i_front],i_front) for i_front in xrange(len(range_front)) )
    min_right,i_right = min( (range_right[i_right],i_right) for i_right in xrange(len(range_right)) )
    min_left ,i_left  = min( (range_left [i_left ],i_left ) for i_left  in xrange(len(range_left )) )
    min_back ,i_back  = min( (range_left [i_back ],i_back ) for i_back  in xrange(len(range_back )) )
    
def comm_callback(msg):
    if(msg.data == "follow"):
        #follow_master()
        print("okay")

    # actually follow...call other script 

# Initialize all variables
range_front = []
range_right = []
range_left  = []
range_back  = []
min_front = 0
i_front = 0
min_right = 0
i_right = 0
min_left = 0
i_left = 0
min_back = 0
i_back = 0

# Get name of the robot
# robot_name = rospy.get_param("/maze_follower/robot_name")
robot_name = rospy.get_param("/maze_detector/robot_name")

# Create the node
cmd_vel_pub = rospy.Publisher(robot_name + '/cmd_vel', Twist, queue_size = 1) # to move the robot
scan_sub = rospy.Subscriber(robot_name + '/scan', LaserScan, scan_callback)   # to read the laser scanner
comm_pub = rospy.Publisher('/comm', String, queue_size=10)
comm_sub = rospy.Subscriber('/comm', String, comm_callback)   # to read the laser scanner

rospy.init_node('maze_detector')

command = Twist()
command.linear.x = 0.0
command.angular.z = 0.0

msg = String()
        
rate = rospy.Rate(10)
time.sleep(2) # wait for node to initialize

master_detected = 0
t = 0.05

while not rospy.is_shutdown():
    # update old values from scanner
    old_front = min_front
    old_right = min_right
    old_left = min_left
    old_back = min_back
    while (master_detected == 0 and not rospy.is_shutdown()):
        if(abs(min_front-old_front)<t and abs(min_left-old_left)<t and abs(min_right-old_right)<t and abs(min_back-old_back)<t):
            pass# print("Looking for master.")
        else:
            comm_pub.publish("hello")
            print("Slave said hello.")
            master_detected = 1
        time.sleep(2)

    else:
        follow = follower.follower()

    rate.sleep()

