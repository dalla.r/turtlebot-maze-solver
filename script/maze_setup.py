#!/usr/bin/env python

import rospy
from gazebo_msgs.srv import DeleteModel, SpawnModel


del_model_prox = rospy.ServiceProxy('gazebo/delete_model', DeleteModel)
spawn_model_prox = rospy.ServiceProxy("gazebo/spawn_model", SpawnModel)

rospy.wait_for_service("gazebo/delete_model")

del_model_prox('Box_Red_clone_3') # Remove from Gazebo