#!/usr/bin/env python

import rospy, time
from sensor_msgs.msg import LaserScan
from geometry_msgs.msg import Twist
from std_msgs.msg import String
import numpy as np
from geometry_msgs.msg import PoseStamped, PoseWithCovarianceStamped
from move_base_msgs.msg import MoveBaseActionGoal
from actionlib_msgs.msg import GoalStatusArray, GoalStatusArray
from pyquaternion import Quaternion

class Navigator:

    def status_callback(self, msg):
        self.status = msg
        self.status = self.status.status_list[self.goal_index].status

    def pose_callback(self, msg):
        self.pose = msg

    def __init__(self):
        rospy.loginfo('Navigation node initialized')
        self.status = 0
        self.goal_index = 0
        self.goal = MoveBaseActionGoal()
        self.goal.goal.target_pose.header.frame_id = "map"
        self.pose = PoseWithCovarianceStamped()
        self.initial_pose = PoseWithCovarianceStamped()
        self.goal_pub = rospy.Publisher('move_base/goal', MoveBaseActionGoal, queue_size=10)
        self.status_sub = rospy.Subscriber('move_base/status', GoalStatusArray, self.status_callback)
        self.pose_sub = rospy.Subscriber('amcl_pose', PoseWithCovarianceStamped, self.pose_callback)
        self.navigation()

    def rotate_in_place(self):         # Rotate in place for better localization
        self.goal.goal.target_pose.pose.orientation = Quaternion([.71, 0, 0, .71])
        self.goal_pub.publish(self.goal)
        while self.status != 3:
            if self.status == 3:
                self.status = 0
                break
            else: pass
        self.goal.goal.target_pose.pose.orientation = Quaternion([0, 0, 0, 1])
        self.goal_pub.publish(self.goal)
        while self.status != 3:
            if self.status == 3:
                self.status = 0
                break
            else: pass
        self.goal.goal.target_pose.pose.orientation = Quaternion([-.71, 0, 0, .71])
        self.goal_pub.publish(self.goal)
        while self.status != 3:
            if self.status == 3:
                self.status = 0
                break
            else: pass
        self.goal.goal.target_pose.pose.orientation = Quaternion([-1, 0, 0, 0])
        self.goal_pub.publish(self.goal)
        while self.status != 3:
            if self.status == 3:
                self.status = 0
                break
            else: pass
        print("Rotation completed.")

    def navigation(self):
        time.sleep(2)
        self.initial_pose.pose.pose.position.x = self.pose.pose.pose.position.x
        self.initial_pose.pose.pose.position.y = self.pose.pose.pose.position.y
        self.goal.goal.target_pose.pose.position.x = self.pose.pose.pose.position.x
        self.goal.goal.target_pose.pose.position.y = self.pose.pose.pose.position.y

        time.sleep(1)


        self.goal.goal.target_pose.pose.position.x = 0
        self.goal.goal.target_pose.pose.position.y = 0
        self.goal.goal.target_pose.pose.orientation = Quaternion([1, 0, 0, 0])
        self.goal_pub.publish(self.goal)
        self.goal_index += 1
        while self.status != 3:
            if self.status == 3:
                self.status = 0
                break
            else: pass
        print("Goal reached!")

        time.sleep(5)

        self.goal.goal.target_pose.pose.position.x = self.initial_pose.pose.pose.position.x
        self.goal.goal.target_pose.pose.position.y = self.initial_pose.pose.pose.position.y
        self.goal_pub.publish(self.goal)
        self.goal_index += 1
        while self.status != 3:
            if self.status == 3:
                self.status = 0
                break
            else: pass
        print("Back home!")

def main():

    rospy.init_node('navigator', anonymous=True)

    try:
        navigation = Navigator()
    except rospy.ROSInterruptException:
        pass

if __name__ == '__main__':
    main()