#!/usr/bin/env python

import rospy, time
from sensor_msgs.msg import LaserScan
from geometry_msgs.msg import Twist
from std_msgs.msg import String
import numpy as np
from geometry_msgs.msg import PoseStamped, PoseWithCovarianceStamped
from move_base_msgs.msg import MoveBaseActionGoal
from actionlib_msgs.msg import GoalStatusArray, GoalStatusArray
from pyquaternion import Quaternion

class return_home:

    def status_callback(self, msg):
        self.status = msg
        if len(self.status.status_list) == 0: self.status = 0
        else:
            self.status = self.status.status_list[len(self.status.status_list)-1].status

    def pose_callback(self, msg):
        self.pose = msg

    def __init__(self):
        rospy.loginfo('Navigation node initialized')
        self.status = 0
        self.robot_name = "tb3_1"
        self.goal = MoveBaseActionGoal()
        self.goal.goal.target_pose.header.frame_id = "map"
        self.pose = PoseWithCovarianceStamped()
        self.initial_pose = PoseWithCovarianceStamped()
        self.goal_pub = rospy.Publisher(self.robot_name + '/move_base/goal', MoveBaseActionGoal, queue_size=10)
        self.status_sub = rospy.Subscriber(self.robot_name + '/move_base/status', GoalStatusArray, self.status_callback)
        self.pose_sub = rospy.Subscriber(self.robot_name + '/amcl_pose', PoseWithCovarianceStamped, self.pose_callback)
    
    def go_to_goal(self, x, y):
        new_goal = MoveBaseActionGoal()
        new_goal.goal.target_pose.header.frame_id = "map"
        new_goal.goal.target_pose.pose.position.x = x
        new_goal.goal.target_pose.pose.position.y = y
        new_goal.goal.target_pose.pose.orientation = Quaternion([0, 0, 0, 1])
        self.goal_pub.publish(new_goal)

    def returner(self):
        time.sleep(1)
        self.goal.goal.target_pose.pose.position.x = 1.8
        self.goal.goal.target_pose.pose.position.y = -0.15
        self.goal.goal.target_pose.pose.orientation = Quaternion([0, 0, 0, 1])
        self.goal_pub.publish(self.goal)

        while self.status != 3 and not rospy.is_shutdown():
            if self.status == 3:
                self.status = 0
                break
            else: pass
        
        print("Back home!")

def main():

    rospy.init_node('return_home', anonymous=True)

    try:
        returner = return_home()
    except rospy.ROSInterruptException:
        pass

if __name__ == '__main__':
    main()