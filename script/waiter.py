#!/usr/bin/env python

import rospy, time
from sensor_msgs.msg import LaserScan
from geometry_msgs.msg import Twist
from std_msgs.msg import String
import numpy as np
from geometry_msgs.msg import PoseStamped, PoseWithCovarianceStamped
from move_base_msgs.msg import MoveBaseActionGoal
from actionlib_msgs.msg import GoalStatusArray, GoalStatusArray
from pyquaternion import Quaternion
import sys

class Waiter:

    def status_callback(self, msg):
        if not msg.status_list:
            self.status = 0
        else:
            self.status = msg.status_list[len(msg.status_list)-1].status

    def pose_callback(self, msg):
        self.pose = msg

    def comm_callback(self, msg):
        self.comm = msg.data


    def __init__(self):
        rospy.loginfo('Waiter node initialized')
        self.status = 0
        self.comm = "Empty"
        self.goal = MoveBaseActionGoal()
        self.goal.goal.target_pose.header.frame_id = "map"
        self.pose = PoseWithCovarianceStamped()
        robot_name = sys.argv[1]
        self.goal_pub = rospy.Publisher(robot_name + '/move_base/goal', MoveBaseActionGoal, queue_size=10)
        self.status_sub = rospy.Subscriber(robot_name + '/move_base/status', GoalStatusArray, self.status_callback)
        self.pose_sub = rospy.Subscriber(robot_name + '/amcl_pose', PoseWithCovarianceStamped, self.pose_callback)
        self.comm_sub = rospy.Subscriber('comm', String, self.comm_callback)
        self.comm_pub = rospy.Publisher('comm', String, queue_size=10)
        self.initialize_poses()
        self.serve()

    def initialize_poses(self):
        self.customer = MoveBaseActionGoal()
        self.water = MoveBaseActionGoal()
        self.food = MoveBaseActionGoal()
        self.napkins = MoveBaseActionGoal()
        self.beer = MoveBaseActionGoal()
        self.customer.goal.target_pose.header.frame_id = "map"
        self.water.goal.target_pose.header.frame_id = "map"
        self.food.goal.target_pose.header.frame_id = "map"
        self.napkins.goal.target_pose.header.frame_id = "map"
        self.beer.goal.target_pose.header.frame_id = "map"


        self.customer.goal.target_pose.pose.position.x = 0.15
        self.customer.goal.target_pose.pose.position.y = 0.0
        self.customer.goal.target_pose.pose.orientation = Quaternion([0, 0, 0, 1])

        self.water.goal.target_pose.pose.position.x = 0.43
        self.water.goal.target_pose.pose.position.y = 0.75
        self.water.goal.target_pose.pose.orientation = Quaternion([-.71, 0, 0, -.71])

        self.food.goal.target_pose.pose.position.x = 0.0
        self.food.goal.target_pose.pose.position.y = 0.4
        self.food.goal.target_pose.pose.orientation = Quaternion([0, 0, 0, 1])

        self.napkins.goal.target_pose.pose.position.x = -0.4
        self.napkins.goal.target_pose.pose.position.y = 0.0
        self.napkins.goal.target_pose.pose.orientation = Quaternion([0, 0, 0, 1])

        self.beer.goal.target_pose.pose.position.x = 0.0
        self.beer.goal.target_pose.pose.position.y = -0.4
        self.beer.goal.target_pose.pose.orientation = Quaternion([0, 0, 0, 1])

    def take_order(self):
        self.comm_pub.publish("What can I get you?")
        while not (self.comm == "Water" or self.comm == "Food" or self.comm == "Napkins" or self.comm == "Beer" and rospy.is_shutdown()):
            pass
        else:
            self.pick_up(self.comm)

    def pick_up(self, order):
        if order == "Water": 
            self.goal = self.water 
        elif order == "Food": 
            self.goal = self.food 
        elif order == "Napkins": 
            self.goal = self.napkins 
        elif order == "Beer": 
            self.goal = self.beer 

        self.goal_pub.publish(self.goal)
        
        while self.status != 3:
            if self.status == 3:
                self.status = 0
                break
            else: 
                while self.comm != "Here it is":
                    pass
                else: 
                    self.deliver() 
                    break

    def deliver(self):
        self.goal = self.customer
        self.goal_pub.publish(self.goal)
        self.status = 0
        while self.status != 3:
            if self.status == 3:
                self.status = 0
                break
            else: pass
        self.comm_pub.publish("Here's your order Sir")

    def serve(self):
        time.sleep(4)
        self.goal = self.customer
        self.goal_pub.publish(self.goal)
        while self.status != 3:
            if self.status == 3:
                self.status = 0
                break
            else: pass

        self.take_order()
        time.sleep(2)

        self.take_order()
        time.sleep(2)

        self.take_order()

def main():

    rospy.init_node('waiter', anonymous=True)

    try:
        waiter = Waiter()
    except rospy.ROSInterruptException:
        pass

if __name__ == '__main__':
    main()